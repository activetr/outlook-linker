========================
Outlook Linker - 1.0.0
========================

Author: James Gibbard (http://jgibbard.me.uk)
Website: http://jgibbard.me.uk/bitbucket/outlook-linker

Outlook Linker provides the ability to open emails in Outlook for Mac 
using a URL (e.g. outlink://outlooklinker?123456). This is helpful 
as Outlook for Mac does not support custom URL links to open messages.

Installation:
--------------
1. Download the app DMG installer (Outlook Linker - http://bit.ly/1eYXi2e)
2. Drag the Outlook linker app to your 'Applications' folder.


Usage:
-------
1. Create a link to a Outlook mail using OutlookTo2do (http://bit.ly/1eYWJFB).
2. Click the link begiging 'outlink://...'
4. Watch your requested mail opens in Outlook - and smile :) 